import nose
from pallanguli import *

def test_player():
    maxseeds = BOARD_SIZE * SEEDS_PER_CUP
    p1 = Player(maxseeds - (SEEDS_PER_CUP-1))
    list(p1.distribute())
    assert p1.store == 1

def test_board_simple():
    player_store = BOARD_SIZE * SEEDS_PER_CUP
    b = Board(Player(player_store),
              Player(player_store))
    
    assert len(b) == BOARD_SIZE * 2, "Board must be twice the length"
    
    for seeds in b:
        assert seeds == SEEDS_PER_CUP

    b[2] = 9
    assert b[BOARD_SIZE*2+2] == b[2]

nose.main()

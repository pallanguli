# the classic Indian pallanguli game AI
# http://library.thinkquest.org/26408/data/texts/34079638.shtml

import sys

__author__ = 'Sridhar Ratnakumar <http://nearfar.org/>'

BOARD_SIZE = 7
SEEDS_PER_CUP = 5

MAXSEEDS = BOARD_SIZE * SEEDS_PER_CUP
MAXDEPTH = 4


class Player(object):

    def __init__(self, store):
        self.store = store # number of seeds on player's store

    def clone(self):
        return Player(self.store)

    def distribute(self):
        "Distribute the seeds from my pristine store to the cups in the board."
        seeds = self.store
        
        if seeds > MAXSEEDS:
            seeds = MAXSEEDS
            
        for index in range(seeds/SEEDS_PER_CUP):
            self.store -= SEEDS_PER_CUP
            yield index

    def give(self, seeds):
        self.store += len(list(seeds))

    def __str__(self):
        return '<Player: %d seeds>' % self.store
        

class Board(list):

    @staticmethod
    def create(player1, player2):
        ll = [None] * (BOARD_SIZE*2)

        for player, offset in [(player1, 0),
                               (player2, BOARD_SIZE)]:
            for index in player.distribute():
                ll[offset + index] = SEEDS_PER_CUP

        return Board(ll)

    def clone(self):
        return Board(self)

    def pick(self, cup):
        "Pick all the seeds from `cup` and yield them one by one"
        seeds = self[cup]
        if seeds == 0:
            raise EmptyCup

        self[cup] = 0
        for index in range(seeds):
            yield index
            
    def next(self, index):
        index += 1
        while self[index] == None:
            index += 1
        return index

    def count(self):
        "Return the count of seeds on both sides of the board"
        return (sum([n for n in self[:BOARD_SIZE] if n is not None]),
                sum([n for n in self[BOARD_SIZE:] if n is not None]))

    def __getitem__(self, index):
        if type(index) is not slice: # __getslice__ too calls this function 
            index = index % len(self)
        return super(Board, self).__getitem__(index)

    def __setitem__(self, index, value):
        index = index % len(self)
        super(Board, self).__setitem__(index, value)

    def __str__(self):
        "Draw the board"
        def fill(stars):
            # return ('*' * stars) + ('_' * (SEEDS_PER_CUP-stars))
            if stars is None:
                return ' X'
            else:
                return '%2d' % stars

        return 'B: %s\n   %s\nA: %s\n' % (
            ' | '.join([fill(n) for n in self[:BOARD_SIZE-1:-1]]),
            '-' * 32,
            ' | '.join([fill(n) for n in self[:BOARD_SIZE]]))


class EmptyCup(Exception): pass

class NoPossibleTurn(Exception): pass

def minimax(game, player, depth):
    if game.endGame(player) or depth == 0:
        return game.evaluate(player)

    if player is True:
        beta = - sys.maxint
        for cgame in game.allMoves(player):
            beta = max(beta, minimax(cgame, not player, depth-1))
        return beta
    else:
        alpha = sys.maxint
        for cgame in game.allMoves(player):
            alpha = min(alpha, minimax(cgame, not player, depth-1))
        return alpha
 

class Game(object):
    """
    The Pallanguli Game object.

    Player A is the good player represented by `True`
    Player B is the bad player represented by `False`
    """

    def __init__(self, pA, pB, board, parentIndex):
        self.pA, self.pB = pA, pB
        self.parentIndex = parentIndex
        self.players = {True: pA, False: pB}
        self.board = board or Board.create(pA, pB)

    def clone(self, index):
        return Game(self.pA.clone(),
                    self.pB.clone(),
                    self.board.clone(),
                    parentIndex = index)

    def cupOwner(self, index):
        if (index % len(self.board)) < BOARD_SIZE:
            return self.players[True]
        else:
            return self.players[False]

    def allMoves(self, playerIndex):
        index = -1
        if playerIndex is False:
            index += BOARD_SIZE

        player = self.players[playerIndex]
        moves = []
        
        while True:
            index = self.board.next(index)
            if self.cupOwner(index) is not player:
                break # we invaded the other player's cups

            if self.board[index] == 0:
                continue # no seeds to pick; skip

            # for each valid cup of mine,
            cgame = self.clone(index)
            cgame.doTurn(index, cgame.players[playerIndex])
            yield cgame

    def endGame(self, playerIndex):
        if playerIndex:
            rack = self.board[:BOARD_SIZE]
        else:
            rack = self.board[BOARD_SIZE:]

        return len([n for n in rack if n is not None and n > 0]) == 0

    def evaluate(self, playerIndex):
        return self.players[True].store - self.players[False].store

    def dopABestTurn(self):
        alpha = - sys.maxint
        bestIndex = None
        for cgame in self.allMoves(True):
            value = minimax(cgame, False, MAXDEPTH)
            if bestIndex is None or value > alpha:
                bestIndex, alpha = cgame.parentIndex, value
        self.doTurn(bestIndex, self.players[True])
        return bestIndex

    def dopBTurn(self, indexOfpB):
        self.doTurn(BOARD_SIZE + indexOfpB, self.players[False])

    def doTurn(self, cup, player):
        "Do turn for player `player` from cup `cup`"
        b = self.board

        assert self.cupOwner(cup) is player, \
            'Player [%s] must move seed from his side only, not #%d' % \
            (player, cup)

        # Turn
        index = cup
        while True:
            # pick seeds from cup `index` and distribute it
            for offset in b.pick(index):
                index = b.next(index)

                if b[index] != None: # valid cup?
                    b[index] += 1

                    if b[index] == 4:
                        self.cupOwner(index).give(b.pick(index))

            index = b.next(index)

            if b[index] == 0:
                index = b.next(index)
                if b[index] > 0:
                    # if the next cup is empty, pick all seeds from
                    # the next-next cup
                    player.give(b.pick(index))
                    b.next(index)
                # stop playing
                break
            else:
                # ..else, continue playing 
                continue

    def __str__(self):
        return '%sA,B: %s, %s\n' % (self.board,
                                    self.players[True],
                                    self.players[False])


def getFromUser(prompt, default):
    q = "%s (%s) " % (prompt, default)
    return raw_input(q).strip() or default

def toBool(userInput):
    if userInput.lower() in ['y', 'yes', 'yep']:
        return True
    if userInput.lower() in ['n', 'no', 'nope']:
        return False
    assert "Invalid userInput for toBool: %s" % userInput

def playPallanguli():
    aSeeds = int(getFromUser("Player A's seeds", MAXSEEDS))
    bSeeds = int(getFromUser("Player B's seeds", MAXSEEDS))
    assert aSeeds + bSeeds == 2*MAXSEEDS
    
    g = Game(Player(aSeeds),
             Player(bSeeds),
             None,
             None)
    print g
    print 'GAME STARTS'

    def askAdversary():
        index = int(getFromUser('Adversary chose:', 1))
        g.dopBTurn(index-1)
        print '--> ADVERSARY picked cup #%d' % index
        print g

    pBStarts = toBool(getFromUser('Adversary starts?', 'n'))
    if pBStarts:
        askAdversary()

    while True:
        index = g.dopABestTurn()
        print '--> YOU picked cup #%d' % (index+1)
        print g

        askAdversary()


if __name__ == '__main__':
    playPallanguli()
